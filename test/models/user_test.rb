require 'test_helper'

class UserTest< ActiveSupport::TestCase

  test 'should create user' do
    @user = User.new
    @user.name = 'lala'
    @user.email = 'lala@gmail.com'
    @user.password = '12345678'
    @user.password_confirmation = '12345678'

    assert @user.save
  end

  test 'should not create user with no name' do
    @user = User.new
    @user.email = 'lala@gmail.com'
    @user.password = '12345678'
    @user.password_confirmation = '12345678'

    assert_not @user.save
  end

  test 'should not create user with no email' do
    @user = User.new
    @user.name = 'aslda'
    @user.password = '12345678'
    @user.password_confirmation = '12345678'

    assert_not @user.save
  end

  test 'should not create user with no password' do
    @user = User.new
    @user.name = 'aslda'
    @user.email = 'lala@gmail.com'
    @user.password_confirmation = '12345678'

    assert_not @user.save
  end

  test 'should not create user with no password confirmation' do
    @user = User.new
    @user.name = 'aslda'
    @user.email = 'lala@gmail.com'
    @user.password = '12345678'

    assert_not @user.save
  end

  test 'should not save user with password and password_confirmation not matching' do
    @user = User.new
    @user.name = 'lala'
    @user.email = 'lala@gmail.com'
    @user.password = '12345678'
    @user.password_confirmation = '12345679'

    assert_not @user.save
  end

  test 'should password not be less than 8 characters' do
    @user = User.new
    @user.name = 'lala'
    @user.email = 'lala@gmail.com'
    @user.password = '12345'
    @user.password_confirmation = '12345678'

    assert_not @user.save
  end

  test 'should password confirmation not be less than 8 characters' do
    @user = User.new
    @user.name = 'lala'
    @user.email = 'lala@gmail.com'
    @user.password = '12345678'
    @user.password_confirmation = '12345'

    assert_not @user.save
  end

  test 'should not save user if email does not have @ character' do
    @user = User.new
    @user.name = 'lala'
    @user.email = 'lalagmail.com'
    @user.password = '12345678'
    @user.password_confirmation = '12345678'

    assert_not @user.save
  end

  test 'should not save user if name has more than 50 characters' do
    @user = User.new
    @user.name = 'lalalalalalalalalalalalalalalalalalalalalalalalalaa'
    @user.email = 'lala@gmail.com'
    @user.password = '12345678'
    @user.password_confirmation = '12345678'

    assert_not @user.save
  end

end