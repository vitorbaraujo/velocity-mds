require 'test_helper'

class UsersControllerTest < ActionController::TestCase

  def setup
    @user = User.new
    @user.name = 'lala'
    @user.email = 'lala@gmail.com'
    @user.password = '12345678'
    @user.password_confirmation = '12345678'
    @user.save
  end

  test 'should get index' do
    get :index
    assert_response :success
  end

  test 'should get new' do
    get :new
    assert_response :success
  end

  test 'should get user page' do
    get :show,id: @user.id
    assert_response :success
  end

  test 'should user be redirected after edit params' do
    get :edit, id: @user.id
    old_name = @user.name
    @user.update_attribute(:name, 'lala2')
    assert_not_equal old_name, @user.name
  end

end